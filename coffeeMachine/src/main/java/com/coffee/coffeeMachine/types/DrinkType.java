package com.coffee.coffeeMachine.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DrinkType {
    TEA("T", 0.4),
    COFFEE("H", 0.6),
    CHOCOLATE("C", 0.5),
    EXTRA_HOT_TEA("Th", 0.4),
    EXTRA_HOT_COFFEE("Hh", 0.6),
    EXTRA_HOT_CHOCOLATE("Ch", 0.5),
    ORANGEJUICE("O", 0.6);

    String drink;
    double price;
}
