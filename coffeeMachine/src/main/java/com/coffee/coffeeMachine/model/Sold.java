package com.coffee.coffeeMachine.model;

import com.coffee.coffeeMachine.types.DrinkType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
public class Sold {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "uuid2")
    UUID id;

    DrinkType drink;

    Date date;

    public Sold(DrinkType drink) {
        this.drink = drink;
        this.date = new Date();
    }
}
