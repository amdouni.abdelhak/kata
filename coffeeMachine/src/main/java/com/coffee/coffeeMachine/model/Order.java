package com.coffee.coffeeMachine.model;

import com.coffee.coffeeMachine.types.DrinkType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Order {
    DrinkType drink;
    int sugar;
    double money;
}
