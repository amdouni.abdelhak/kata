package com.coffee.coffeeMachine.repository;

import com.coffee.coffeeMachine.model.Sold;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SoldRepository extends JpaRepository<Sold, UUID> {
}
