package com.coffee.coffeeMachine.service;

import com.coffee.coffeeMachine.model.Order;
import com.coffee.coffeeMachine.model.Sold;
import com.coffee.coffeeMachine.repository.SoldRepository;
import com.coffee.coffeeMachine.types.DrinkType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TranslatorService {

    private final SoldRepository soldRepository;
    private final BeverageQuantityChecker beverageQuantityChecker;
    private final EmailNotifier emailNotifier;

    @Autowired
    public TranslatorService(SoldRepository soldRepository, BeverageQuantityChecker beverageQuantityChecker, EmailNotifier emailNotifier) {
        this.soldRepository = soldRepository;
        this.beverageQuantityChecker = beverageQuantityChecker;
        this.emailNotifier = emailNotifier;
    }

    public String translateOrder(Order order) {
        String orderString = "";

        if(beverageQuantityChecker.isEmpty(order.getDrink().getDrink())){
            String message = "we can't deliver the drink because of a shortage";
            emailNotifier.notifyMissingDrink(message);
            return "M:"+message;
        }

        if(order.getMoney() < order.getDrink().getPrice())
            return "M:Not enough money messing " + (order.getDrink().getPrice() - order.getMoney());
        orderString+=order.getDrink().getDrink();

        if (order.getSugar() != 0) {
            orderString = orderString + ":" + order.getSugar() + ":0";
        } else {
            orderString = orderString + "::";
        }

        Sold sold = new Sold(order.getDrink());
        soldRepository.save(sold);
        return orderString;
    }

    public Map<String, Double> report(){

        Map<String, Double> reportSales = new HashMap<>();

        List<Sold> solds = this.soldRepository.findAll();
        Map<DrinkType, List<Sold>> groupedSolds = solds.stream().collect(Collectors.groupingBy(Sold::getDrink));

        double totalamount = solds.stream().map(Sold::getDrink).map(DrinkType::getPrice)
                .collect(Collectors.summingDouble(Double::doubleValue));

        for( DrinkType drinkType: groupedSolds.keySet()) {
            reportSales.put(drinkType.getDrink(), (double) groupedSolds.get(drinkType).size());
        };
        reportSales.put("total", totalamount);
       return reportSales;
    }
}
