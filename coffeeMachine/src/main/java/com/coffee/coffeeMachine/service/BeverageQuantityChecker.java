package com.coffee.coffeeMachine.service;

import org.springframework.stereotype.Service;

@Service
public interface BeverageQuantityChecker {
    boolean isEmpty(String drink);
}
