package com.coffee.coffeeMachine.service;

public class EmailNotifierImpl implements EmailNotifier {

    @Override
    public void notifyMissingDrink(String drink) {
        System.out.println(drink.substring(drink.indexOf("M:"+2)));
    }
}
