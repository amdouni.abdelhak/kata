package com.coffee.coffeeMachine.service;

import org.springframework.stereotype.Service;

@Service
public interface EmailNotifier {
    void notifyMissingDrink(String drink);
}
