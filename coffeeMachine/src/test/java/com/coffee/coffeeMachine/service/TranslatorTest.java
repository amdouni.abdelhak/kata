package com.coffee.coffeeMachine.service;

import com.coffee.coffeeMachine.model.Order;
import com.coffee.coffeeMachine.model.Sold;
import com.coffee.coffeeMachine.repository.SoldRepository;
import com.coffee.coffeeMachine.types.DrinkType;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class TranslatorTest extends MockitoExtension {

    @MockBean
    SoldRepository soldRepository;

    @MockBean
    EmailNotifier emailNotifier;

    @MockBean
    BeverageQuantityChecker beverageQuantityChecker;

    @Autowired
    private TranslatorService translatorService;

    @Test
    public void OrderTranslator(){

        Order orderCoffee = new Order(DrinkType.COFFEE,2, 1);
        Order orderTea = new Order(DrinkType.TEA,0, 2);
        Order orderChocolate = new Order(DrinkType.CHOCOLATE, 1, 3);
        Order orderChocolateWithNotEnoughMoney = new Order(DrinkType.CHOCOLATE, 1, 0.2);
        Order orderExtraHotCoffee = new Order(DrinkType.EXTRA_HOT_COFFEE,2, 1);
        Order orderExtraHotTea = new Order(DrinkType.EXTRA_HOT_TEA,0, 2);
        Order orderExtraHotChocolate = new Order(DrinkType.EXTRA_HOT_CHOCOLATE, 1, 3);
        Order orderOrangeJuice = new Order(DrinkType.ORANGEJUICE,0, 0.6);

        ArrayList<Sold> solds = Lists.newArrayList(
                new Sold(DrinkType.ORANGEJUICE),
                new Sold(DrinkType.CHOCOLATE),
                new Sold(DrinkType.COFFEE)
        );

        Mockito.when(soldRepository.findAll()).thenReturn(solds);

        assertEquals("H:2:0", translatorService.translateOrder(orderCoffee));
        assertEquals("T::", translatorService.translateOrder(orderTea));
        assertEquals("C:1:0", translatorService.translateOrder(orderChocolate));
        assertEquals("M:Not enough money messing 0.3", translatorService.translateOrder(orderChocolateWithNotEnoughMoney));
        assertEquals("Hh:2:0", translatorService.translateOrder(orderExtraHotCoffee));
        assertEquals("Th::", translatorService.translateOrder(orderExtraHotTea));
        assertEquals("Ch:1:0", translatorService.translateOrder(orderExtraHotChocolate));
        assertEquals("O::", translatorService.translateOrder(orderOrangeJuice));
        Map<String, Double> report = translatorService.report();

        assertEquals(report.get("C"), 1);
        assertEquals(report.get("O"), 1);
        assertEquals(report.get("H"), 1);
        assertEquals(report.get("total"), 1.7);

        Mockito.when(beverageQuantityChecker.isEmpty(any())).thenReturn(true);

        assertEquals("M:we can't deliver the drink because of a shortage", translatorService.translateOrder(orderOrangeJuice));

    }

}
